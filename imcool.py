import csv
import sys
import MySQLdb # sudo apt-get install python-mysqldb

def main():
  update_all_flash_embed_urls()

# replace all collegehumor flash embed urls with the updated html5 compatible versions
# clip_id is the old flash id
# node_id is the new html5 id
def update_all_flash_embed_urls():
  id_map_data = get_id_map_data()

  connection = MySQLdb.connect(host="localhost", user="root", db="janda")
  cursor = connection.cursor()
  
  for row in id_map_data:
    clip_id = row[0]
    node_id = row[1]
    title = row[2]

    execution_sql = get_replacement_sql(clip_id, node_id)
    cursor.execute(execution_sql)
    execution_sql = get_replacement_using_node_id_sql(node_id)
    cursor.execute(execution_sql)
    connection.commit()
    print '%s processed' % title

# return a SQL string that updates embed_src of the following form:
# http://www.collegehumor.com/moogaloop/moogaloop.swf?clip_id=1766580
def get_replacement_sql(clip_id, node_id):
  old_embed_src = 'moogaloop.swf?clip_id='+clip_id
  new_embed_src = 'http://collegehumor.com/e/'+node_id
  return 'UPDATE episodes SET embed_src="'+new_embed_src+'" WHERE embed_src LIKE "%'+old_embed_src+'%"'

# return a SQL string that updates embed_src of the following form:
# http://www.collegehumor.com/moogaloop/moogaloop.swf?clip_id=6475612&use_node_id=true
def get_replacement_using_node_id_sql(node_id):
  old_embed_src = 'moogaloop.swf?clip_id='+node_id+'&use_node_id=true'
  new_embed_src = 'http://collegehumor.com/e/'+node_id
  return 'UPDATE episodes SET embed_src="'+new_embed_src+'" WHERE embed_src LIKE "%'+old_embed_src+'%"'

# return an array of tuples containing the csv values 
# [..., (clip_id, node_id, title), ...]
def get_id_map_data():
  filename = 'id_map.csv'
  ifile = open(filename, 'rU')
  csv_stream = csv.reader(ifile)

  row_number = 0
  id_map_data = []
  for row in csv_stream:
    if  row_number != 0:
      clip_id = row[0]
      node_id = row[1]
      title = row[2]
      id_map_data.append((clip_id, node_id, title))
    row_number += 1

  ifile.close()
  return id_map_data

# execute main function
if  __name__ =='__main__':main()