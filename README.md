# J&A Private Tools #

* Tools for updating the database locally
* Previous and new SQL dumps
* Anything we don't want the world to see in a public github account

## Useful commands ##

```
#!bash
mysql -h localhost -u root janda < existing_data_dump.sql

```

```
#!bash
mysqldump janda episodes > new_dump_2014_06_04.sql

```