# -*- coding: utf-8 -*-

import csv 
from urlgrabber import urlopen
from HTMLParser import HTMLParser
import sys
import MySQLdb
import re
#f = open('brownie.txt', 'r').read()

# return the entire page's html
def getScriptHTML(url):  
    #print 'processing %s...' % url
    try: fo = urlopen(url, timeout=20)
    except URLError, e:
      print "error getting script!"
    pageHtml = fo.read()
    fo.close()
    return pageHtml

# parser to find the stripped script text
class scriptFinder(HTMLParser):
  def __init__(self):
    HTMLParser.__init__(self)
    self.in_div = False
    self.div_num = 0
    self.scriptText = ''

  def handle_starttag(self, tag, attrs):
    if tag == 'div' and len(attrs) > 0:
      pair = attrs[0]
      if pair == ('class', 'expando'):  
        self.in_div = True
        self.div_num = self.div_num + 1

  def handle_data(self, data):
    if self.in_div:
      self.scriptText += data

  def handle_endtag(self, tag):
    if tag == 'div' and self.in_div:
      self.div_num = self.div_num - 1
      if self.div_num == 0:
        self.in_div = False
  
  def get_data(self):
    return self.scriptText

# parser to strip tags (not used)
class MLStripper(HTMLParser):
    def __init__(self):
        self.reset()
        self.fed = []
    def handle_data(self, d):
        self.fed.append(d)
    def get_data(self):
        return ''.join(self.fed)


def getScriptText(scriptHTML):
    s = scriptFinder()
    s.feed(scriptHTML)
    return s.get_data()

# handle all cases to retrieve embed-able src attr
def extractSrcFromLink(link):
  src = -1

  # http://www.collegehumor.com/video/6658699/fiat-roadtrip 
  if link.find('collegehumor.com') >= 0:
    uid = link.split('video/')[1]
    uid = uid.split('/')[0]
    src = 'http://www.collegehumor.com/e/'+uid

  # http://vimeo.com/175672 
  if link.find('vimeo.com') >= 0:
    uid = link.split('vimeo.com/')[1]
    src = 'http://player.vimeo.com/video/'+uid

  # http://www.youtube.com/watch?v=P0iOFhJmiCI  
  if link.find('youtube.com') >= 0:
    uid = link.split('v=')[1]
    src = 'http://www.youtube.com/embed/'+uid

  # http://www.facebook.com/photo.php?v=155621744964
  if link.find('facebook.com') >= 0:
    uid = link.split('v=')[1]
    src = 'http://www.facebook.com/video/embed?video_id='+uid

  # get src from iframe in tumblr post
  if link.find('jakeandamir.com') >= 0:
    html = getScriptHTML(link)
    s = srcFinder()
    s.feed(html)
    src = s.get_data()

  return src

# parser to find the embed src 
class srcFinder(HTMLParser):
  def __init__(self):
    HTMLParser.__init__(self)
    self.src = ''
    self.done = False

  def handle_starttag(self, tag, attrs):
    if (tag == 'iframe' or tag == 'embed') and len(attrs) > 0 and self.done == False:
      embedUrl = attrs[0][1]
      # embedded url is already correctly formatted
      if embedUrl.find('collegehumor.com') >= 0 or embedUrl.find('vimeo.com') >= 0:
        self.src = embedUrl.encode('utf-8')
        self.done = True
    if tag == 'object' and len(attrs) > 0 and self.done == False:
      for attrPair in attrs:
        if attrPair[0] == 'id' and attrPair[1].find('ch') == 0:
          uid = attrPair[1].split('ch')[1]
          embedUrl = 'http://www.collegehumor.com/e/' + uid
          self.src = embedUrl.encode('utf-8')
          self.done = True
        if attrPair[0] == 'data':
          embedUrl = attrPair[1]     
          if embedUrl.find('collegehumor.com') >= 0:
            self.src = embedUrl.encode('utf-8')
            self.done = True

  def get_data(self):
    return self.src

# strip html tags (not used)
def stripHTML(scriptDiv):
    s = MLStripper()
    s.feed(scriptDiv)
    return s.get_data()

def addSpaces(scriptText):
    return scriptText.replace('\n','<br />\n')    

def formatTicks(scriptHtml):
    scriptHtml = scriptHtml.replace("’", "'")
    scriptHtml = scriptHtml.replace("‘", "'")
    scriptHtml = scriptHtml.replace("…", "...")
    scriptHtml = scriptHtml.replace("–", "-")
    scriptHtml = scriptHtml.replace('“', re.escape('\"'))
    scriptHtml = scriptHtml.replace('”', re.escape('\"'))
    scriptHtml = scriptHtml.replace("é", "e")

    return scriptHtml.replace('&#39;', "'")

def parseCSV():    
  #ifile  = open('jake_amir_checklist.csv', "rb")
  #ifile  = open('jake_amir_checklist_3.csv', "rb")
  ifile = open("jake_amir_wiki_checklist.csv", "rb")
  #ifile  = open('short_list.csv', "rb")
  reader = csv.reader(ifile)
    
  con = MySQLdb.connect(host="localhost", user="root", db="janda")
  cursor = con.cursor() 
  cursor.execute('SET NAMES utf8')
  con.commit()

  rownum = 0
  for row in reader:
    # Save header row.
    if rownum == 0:
      header = row
    else:
      colnum = 0
      title = row[0]
      status = row[1]
      scriptLink = row[2]
      user = row[3]
      if status != 'Wiki':
        script = addSpaces(getScriptText(formatTicks(getScriptHTML(scriptLink))))
        cursor.execute('SELECT link FROM episodes WHERE title="'+title+'"')
        titleFetch = cursor.fetchone()
        if titleFetch is None:
          print '%s no SQL row - ignored' % title
          embedSrc = ""
        else:
          episodeLink = titleFetch[0]
          embedSrc = extractSrcFromLink(episodeLink)
        #print 'embedSrc %s' % embedSrc
        cursor.execute('UPDATE episodes SET script="'+script+'", scribe="'+user+'", embed_src="'+embedSrc+'" WHERE title="'+title+'"')        
        con.commit() 
        print '%s processed' % title
      else:
        print '%s wiki - ignored' % title
     
    rownum += 1

  ifile.close()



parseCSV()

#print extractSrcFromLink('http://vimeo.com/175672')

#print extractSrcFromLink('http://www.jakeandamir.com/post/72088207/new-girlfriend')
#print extractSrcFromLink('http://www.jakeandamir.com/post/116904079/dating-coach')
#print extractSrcFromLink('http://www.facebook.com/photo.php?v=5894524964')
#print extractSrcFromLink('http://vimeo.com/175672')
#print extractSrcFromLink('http://www.collegehumor.com/video/6658699/fia')
#print extractSrcFromLink('http://www.jakeandamir.com/post/17218222565/traffic')

#print 'Stripped HTML: %s' % stripHTML(getScriptHTML('http://www.reddit.com/r/JakeAndAmirScripts/comments/1a48nb/070731_hang_up/'))
#print '%s' % getScriptText(getScriptHTML('http://www.reddit.com/r/JakeAndAmirScripts/comments/1a48nb/070731_hang_up/'))
#print '%s' % getScriptText(formatTicks(getScriptHTML('http://www.reddit.com/r/JakeAndAmirScripts/comments/1a48nb/070731_hang_up/')))

#con = MySQLdb.connect(host="192.168.1.20", user="mithos", passwd="martel", db="janda")
#cursor = con.cursor() 
#scip =  addSpaces(getScriptText(getScriptHTML('http://www.reddit.com/r/JakeAndAmirScripts/comments/1a48nb/070731_hang_up/')))
#tite = 'Hang Up'
#dude = 'me (for now)'
#cursor.execute("UPDATE episodes SET script='"+scip+"', scribe='"+dude+"' WHERE title='"+tite+"'")
#con.commit() 

